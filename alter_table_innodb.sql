DELIMITER $$

USE `common_schema`$$

DROP VIEW IF EXISTS `alter_table_innodb`$$

CREATE ALGORITHM=UNDEFINED SQL SECURITY INVOKER VIEW `alter_table_innodb` AS 
SELECT
  `information_schema`.`TABLES`.`TABLE_SCHEMA` AS `TABLE_SCHEMA`,
  `information_schema`.`TABLES`.`TABLE_NAME`   AS `TABLE_NAME`,
  `information_schema`.`TABLES`.`ENGINE`       AS `ENGINE`,
  /*CONCAT('ALTER TABLE ',`mysql_qualify`(`information_schema`.`TABLES`.`TABLE_SCHEMA`),'.',`mysql_qualify`(`information_schema`.`TABLES`.`TABLE_NAME`),' ENGINE='INNODB') AS `alter_statement` */
  concat('ALTER TABLE ',TABLE_SCHEMA,'.',TABLE_NAME,' ENGINE=InnoDB;') as `ALTER_TABLE_STATEMENT`
FROM `INFORMATION_SCHEMA`.`TABLES`
WHERE ((`information_schema`.`TABLES`.`TABLE_SCHEMA` NOT IN('mysql', 'INFORMATION_SCHEMA', 'performance_schema','common_schema','test')) 
AND (`information_schema`.`TABLES`.`ENGINE`='MyISAM'))
ORDER BY `information_schema`.`TABLES`.`TABLE_NAME`$$

DELIMITER ;