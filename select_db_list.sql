DELIMITER $$

USE `common_schema`$$

DROP VIEW IF EXISTS `select_db_list`$$

CREATE ALGORITHM=UNDEFINED SQL SECURITY INVOKER VIEW `select_db_list` AS 
SELECT
  `information_schema`.`SCHEMATA`.`SCHEMA_NAME` AS `TABLE_SCHEMA`
FROM `INFORMATION_SCHEMA`.`SCHEMATA`
WHERE `information_schema`.`SCHEMATA`.`SCHEMA_NAME` NOT IN('mysql','INFORMATION_SCHEMA','performance_schema','common_schema','test')
ORDER BY `information_schema`.`SCHEMATA`.`SCHEMA_NAME`$$

DELIMITER ;