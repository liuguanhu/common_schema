
查询数据库没有主键的表

SELECT table_schema,table_name,ENGINE,candidate_keys FROM no_pk_tables WHERE table_schema='gamedb';

查询数据库没有主键的表(INNODB)

SELECT table_schema,table_name,ENGINE,candidate_keys FROM no_pk_innodb_tables WHERE table_schema='gamedb';

查询数据库外键

SELECT table_schema,table_name,drop_statement,create_statement FROM sql_foreign_keys WHERE table_name='gamedb';

查看索引唯一性

SELECT * FROM candidate_keys WHERE table_schema='gamedb' ORDER BY candidate_key_rank_in_table;

查看数据库自增字段

SELECT * FROM auto_increment_columns  WHERE TABLE_SCHEMA='gamedb';


查询数据库重复索引

SELECT  table_schema,table_name,redundant_index_name,redundant_index_columns,dominant_index_name,dominant_index_columns,CONCAT(sql_drop_index,';') FROM redundant_keys WHERE table_schema='gamedb';

查询数据库的字符集

SELECT table_schema,table_name,character_set_name,table_collation FROM table_charset WHERE table_schema='gamedb';

查看数据库所有索引

SELECT * FROM _sql_table_keys WHERE TABLE_SCHEMA='gamedb';

查询数据库唯一索引

SELECT * FROM _unique_keys WHERE table_schema='gamedb' AND is_primary=1 ORDER BY table_name;

查询数据库主键索引

SELECT table_name,table_name,index_name,COUNT_COLUMN_IN_INDEX,is_primary,column_names,first_column_name FROM _unique_keys WHERE table_schema='gamedb' AND is_primary=1 ORDER BY table_name;

查询数据库每个表大小

SELECT table_name,ENGINE,total_size_mb FROM data_size_per_table WHERE table_schema='gamedb';

数据库引擎转换(MyISAM转为INNODB)

SELECT table_schema,table_name,ENGINE,alter_table_statement FROM alter_table_innodb WHERE table_schema='gamedb';



sed -i  "s#TABLE_SCHEMA NOT IN ('mysql', 'INFORMATION_SCHEMA', 'performance_schema')#TABLE_SCHEMA NOT IN ('mysql', 'INFORMATION_SCHEMA', 'performance_schema','common_schema','test')##g" common_schema-2.2.sql 




