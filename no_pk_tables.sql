DELIMITER $$

USE `common_schema`$$

DROP VIEW IF EXISTS `no_pk_tables`$$

CREATE ALGORITHM=UNDEFINED SQL SECURITY INVOKER VIEW `no_pk_tables` AS 
SELECT
  `information_schema`.`TABLES`.`TABLE_SCHEMA` AS `TABLE_SCHEMA`,
  `information_schema`.`TABLES`.`TABLE_NAME`   AS `TABLE_NAME`,
  `information_schema`.`TABLES`.`ENGINE`       AS `ENGINE`,
  GROUP_CONCAT(IF((`information_schema`.`TABLE_CONSTRAINTS`.`CONSTRAINT_TYPE` = 'UNIQUE'),`information_schema`.`TABLE_CONSTRAINTS`.`CONSTRAINT_NAME`,NULL) SEPARATOR ',') AS `candidate_keys`
FROM (`INFORMATION_SCHEMA`.`TABLES`
   LEFT JOIN `INFORMATION_SCHEMA`.`TABLE_CONSTRAINTS`
     ON (((`information_schema`.`TABLES`.`TABLE_SCHEMA` = `information_schema`.`TABLE_CONSTRAINTS`.`TABLE_SCHEMA`)
          AND (`information_schema`.`TABLES`.`TABLE_NAME` = `information_schema`.`TABLE_CONSTRAINTS`.`TABLE_NAME`))))
WHERE `information_schema`.`SCHEMATA`.`SCHEMA_NAME` NOT IN('mysql','INFORMATION_SCHEMA','performance_schema','common_schema','test')
GROUP BY `information_schema`.`TABLES`.`TABLE_SCHEMA`,`information_schema`.`TABLES`.`TABLE_NAME`,`information_schema`.`TABLES`.`ENGINE`
HAVING (IFNULL(SUM((`information_schema`.`TABLE_CONSTRAINTS`.`CONSTRAINT_TYPE` = 'PRIMARY KEY')),0) = 0)$$

DELIMITER ;