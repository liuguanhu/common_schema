DELIMITER $$

USE `common_schema`$$

DROP VIEW IF EXISTS `data_size_per_table`$$

CREATE ALGORITHM=UNDEFINED SQL SECURITY INVOKER VIEW `data_size_per_table` AS 
SELECT
  `information_schema`.`TABLES`.`TABLE_SCHEMA` AS `TABLE_SCHEMA`,
  `information_schema`.`TABLES`.`TABLE_NAME`   AS `table_name`,
  `information_schema`.`TABLES`.`ENGINE`       AS `ENGINE`,
  SUM((`information_schema`.`TABLES`.`TABLE_TYPE` = 'BASE TABLE')) AS `count_tables`,
  SUM((`information_schema`.`TABLES`.`TABLE_TYPE` = 'VIEW')) AS `count_views`,
  COUNT(DISTINCT `information_schema`.`TABLES`.`ENGINE`) AS `distinct_engines`,
  ((SUM((`information_schema`.`TABLES`.`DATA_LENGTH` + `information_schema`.`TABLES`.`INDEX_LENGTH`)) / 1024) / 1024) AS `total_size_mb`
FROM `INFORMATION_SCHEMA`.`TABLES`
WHERE (`information_schema`.`TABLES`.`TABLE_SCHEMA` NOT IN('mysql', 'INFORMATION_SCHEMA', 'performance_schema','common_schema','test'))
GROUP BY `information_schema`.`TABLES`.`TABLE_NAME`
ORDER BY `total_size_mb` DESC$$

DELIMITER ;